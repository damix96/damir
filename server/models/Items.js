var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;

var Item = new Schema({
    title: {type: String, default: 'No Title'},
    description: {type: String, default: 'No Description'},
    photo: {type: String, default: '/content/noimage.png'},
    count: {type: String, default: 0},
    price: {type: String, default: 0},
    likes: {type: Number, default: 0},
    category: {type: String, default: 'No Category'},
    openTime: {type: Date, default: Date.now}
},{
    timestamp:true
});

module.exports = mongoose.model('Item', Item);
