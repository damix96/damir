var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;

var User = new Schema({
    login: {type: String, unique: true},
    password: {type: String},
    admin: {type: Boolean, default: false},
    cart: [{type: mongoose.Schema.Types.ObjectId, ref: 'Item'}],
    token: {type: String}//{type: mongoose.Schema.Types.ObjectId, ref: 'Access'}
},{
    timestamp:true
});

module.exports = mongoose.model('User', User);
