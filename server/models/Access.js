var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;

var Access = new Schema({
    token: { type: String, unique: true },
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    created: { type: Date, default: Date.now, expires: 86400*5 }
},{
    timestamp:true
});

module.exports = mongoose.model('Access', Access);
