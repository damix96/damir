var express = require('express');
// var moment = require('moment');
var router = express.Router();
var Items = require('../models/Items');
var Users = require('../models/Users');
var Access = require('../models/Access');
const { saveTo } = require('../SaveFrom64');
/// Access <>
var { checkAccess, getUserInfo } = require('./Access');
/// Access </>

router.get('/all', (req, res) => {
  const { token } = req.cookies;
  console.log('[Items All] =>', token);
  getUserInfo(token, function (err, user) {
    if(err||!user){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      Items.find({}, '-__v').exec(function (err, items) {
        res.send({success: true, data: items})
      })
    }
  })
})

router.post('/add', (req, res) => {
  const { token } = req.cookies;
  const { price, title, description, photo, count, category } = req.body;
  console.log('[Items Add] =>', title, count, category);
  getUserInfo(token, function (err, user) {
    if(err||!user||!user.admin){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      new Items({
        title,
        description,
        count,
        price,
        category
      }).save(function (err, item) {
        Items.findOne({_id: item._id}).exec(function (err, Item) {
          if(photo){
            var photoPath = '/content/'+item._id;
            saveTo(photo, '/static'+photoPath, '.jpg', function (err, saved) {
              Item.photo = photoPath+'.jpg';
              Item.save(function (err) {
                res.send({success: true, msg: 'Товар успешно добавлен'});
              })
            });
          } else {
            res.send({success: true, msg: 'Товар успешно добавлен'});
          }
        })
      })
    }
  })
})

router.post('/edit', (req, res) => {
  const { token } = req.cookies;
  const { _id, price, title, description, photo, count, category } = req.body;
  console.log('[Items Edit] =>', token);
  getUserInfo(token, function (err, user) {
    if(err||!user||!user.admin){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      Items.findOne({_id})
      .exec(function (err, Item) {
        Item.price = price||Item.price;
        Item.title = title||Item.title;
        Item.description = description||Item.description;
        Item.count = count||Item.count;
        Item.category = category||Item.category;
        if(photo){
          var photoPath = '/content/'+Item._id;
          Item.photo = photoPath+'.jpg';
          saveTo(photo, '/static'+photoPath, '.jpg');
        }

        Item.save(function (err, saved) {
          if(err) {
            res.send({success: false, msg: 'Ошибка базы данных.'});
            return;
          }
          res.send({success: true, msg: 'Изменения сохранены', data: saved});
        })
      })
    }
  })
})

router.post('/remove', (req, res) => {
  const { token } = req.cookies;
  const { _id } = req.body;
  console.log('[Items Remove] =>', token);
  getUserInfo(token, function (err, user) {
    if(err||!user||!user.admin){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      Items.findOneAndRemove({_id})
      .exec(function (err, Item) {
          if(err) {
            res.send({success: false, msg: 'Ошибка базы данных.'});
            return;
          }
          if(Item){
            res.send({success: true, msg: 'Запись успешно удалена'});
          } else {
            res.send({success: false, msg: 'Запись не найдена.'});
          }
      })
    }
  })
})

router.post('/tocart', (req, res) => {
  const { token } = req.cookies;
  const { _id  } = req.body;
  console.log('[Items To Cart] =>', token);
  getUserInfo(token, function (err, user) {
    if(err||!user){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      Users.findOne({token}).exec(function (err, User) {
        var index = User.cart.indexOf(_id);
        var response = {success: false, msg: 'Что-то пошло не так.'}
        if( index > -1){
          response = {success:false, msg: 'Этот товар больше не в корзине!'}
          User.cart.splice(index,1);
        } else {
          response = {success:true, msg: 'Товар добавлн в корзину!'}
          User.cart.push(_id);
        }
        User.save(function (err, saved) {
          console.log(err, saved);
          if(err||!saved){
            res.send({success:false, msg: 'Ошибка базы данных'})
          }
          if(saved) {
            res.send(response)
          }
        })
      })
    }
  })
})

module.exports = router
