var express = require('express');
var router = express.Router();
var Users = require('../models/Users');
var Access = require('../models/Access');
/// Access <>
var { makeHash, newAccess, checkAccess, getUserInfo, removeAccess, dropAccess, adminAccess } = require('./Access');
/// Access </>

/// auth <> DONE

function newUser(login, password, callBack){
  new Users({login, password}).save(function (err, user) {
    if(err){
      callBack(false)
    } else {
      callBack(true)
    }
  })
}

router.get('/logout', (req, res) => {
  const { token } = req.cookies;
  removeAccess(token, function () {
    res.cookie('token', null);
    res.send({success: true})
  })
})

router.post('/auth', (req, res) => {
  console.log('[Auth] =>',req.body);
  removeAccess(req.cookies.token);
  const { login, password } = req.body;
  newAccess(login, password, function (err, token) {
    if(err){
      res.send({success: false, msg: 'Неверный логин или пароль'})
      return
    }
    res.cookie('token', token);
    res.send({success: true, msg: 'Красава'})
  })
})

router.post('/reg', (req, res) => {
  console.log('[Register] =>',req.body);
  removeAccess(req.cookies.token);
  const { login, password } = req.body;
  newUser(login, password, function (success) {
    if(success){
      newAccess(login, password, function (err, token) {
        if(err) {
          res.send({success: false, msg: 'Ошибка базы данных'})
          return
        }
        res.cookie('token', token);
        res.send({success: true, msg: 'Зарегался'})
      })
    } else {
      res.send({success: false, msg: 'Данный логин уже занят.'})
    }
  })


})

/// auth </> DONE

router.get('/mydata', (req, res) => {
  const { token } = req.cookies;
  console.log('[Profile] =>', token);
  getUserInfo(token, function (err, user) {
    if(err||!user){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      res.send({success: true, data: user});
    }
  })
})

router.post('/switch', (req, res) => {
  const { token } = req.cookies;
  const { admin } = req.body;
  console.log('[Profile] Admin =>', admin);
  getUserInfo(token, function (err, user) {
    if(err||!user){
      res.send({success: false, msg: 'Недостаточно прав.'});
    } else {
      adminAccess(token, admin, function (err, saved) {
        res.send({success: true, data: saved});
      })
    }
  })
})





















/*
/// faq <>

router.get('/faq', (req, res) => {
  const QUERY = '-_id answer question title'
  Questions.find({inFAQ:true}, QUERY).exec(function (err, faqs) {
    if(err) throw err
    res.status(200).send(faqs);
  })
})

router.post('/faq', (req, res) => {
  const token = req.cookies.token||req.body.token;
  checkAccess(token, function ( passed ) {
    if(passed) {
      Questions.find({ inFAQ:true }, '-__v').exec(function (err, faqs) {
        if(err) throw err
        res.send({success: true, msg: 'OK', data: faqs});
      })
    } else {
      res.send({success: false, msg: 'Недостаточно прав.', data: []});
    }
  })
})

router.post('/deletefaq', (req, res) => {
  const {
    token = req.cookies.token,
    _id
  } = req.body;
  checkAccess(token, function ( passed ) {
    if(passed) {
      Questions.findOneAndRemove({ _id }).exec(function (err, removed) {
        if(err) {
          res.send({success: false, msg: 'Ошибка базы данных'});
          return;
        }
        if(removed){
          res.send({success: true, msg: 'OK', data: removed});
        } else {
          res.send({success: false, msg: 'Запись не найдена.'});
        }
      })
    } else {
      res.send({success: false, msg: 'Недостаточно прав.', data: []});
    }
  })
})

router.post('/editfaq', (req, res) => {
  const {
    token = req.cookies.token,
    _id,
    question,
    answer,
    title,
    inFAQ,
    open
  } = req.body;
  checkAccess(token, function ( passed ) {
    if(passed) {
      Questions.findOne({ _id }).exec(function (err, faq) {
        if(err) throw err
        if(faq){
          faq.question = question||faq.question;
          faq.answer = answer||faq.answer;
          faq.title = title||faq.title;
          faq.open = assign(open, faq.open);
          faq.inFAQ = assign(inFAQ, faq.inFAQ);
          faq.save(function (err, saved) {
            if(err){
              res.send({success: false, msg: 'Ошибка при сохранении изменений'});
            } else {
              res.send({success: true, msg: 'OK', data: saved});
            }
          })
        } else {
          res.send({success: false, msg: 'ID ['+_id+'] не найден'});
        }

      })
    } else {
      res.send({success: false, msg: 'Недостаточно прав.', data: []});
    }
  })
})
/// faq </>

*/


module.exports = router
