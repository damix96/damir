var express = require('express');
var router = express.Router();
var Users = require('../models/Users');
var Access = require('../models/Access');

const HASH_LENGTH = 15;


/// Access <>
function makeHash(n){
  var n = n ? (n > 0 ? n : 1) : 1;
  var alp = ['a','0','b','c','1','d','e','f','g','2','3','4','h','i','5','j','6','7','k','l','m','n','8','o','p','q','r','s','t','u','v','w','x','y','9','z']
  var r = ''
  for (var i = 0; i < n; i++) {
    var q = Math.round(Math.random() * alp.length-1)
    if(q<0)q=0;
    r = r+alp[q]
  }
  return r;
}

function newAccess( login, password, callBack ) {
  Access.find({}, '-_id -created -__v').exec(function (err, tokens) {
    if( err ) {
      newAccess( login, password, callBack );
      return;
    }
    var token = makeHash(HASH_LENGTH);
    while (tokens.indexOf(token) > 0) {
      token = makeHash(HASH_LENGTH);
    };
    Users.findOne({login, password}).exec(function (err, user) {
      if(err){
        newAccess( login, password, callBack );
        return;
      }
      if(user){
        new Access({token}).save(function (err, access) {
          if(err){
            callBack(true)
            return
          }
          console.log(err, access);
          user.token = token;
          user.save(function () {
            callBack(null, token);
          });
        })
      } else {
        callBack(true)
      }
    })
  })
}

function checkAccess( token, callBack ) {
  if(!token) {
    callBack(false);
    return;
  }
  Access.findOne({token}).exec(function ( err, found ) {
    if(err) {
      checkAccess( token, callBack );
      return;
    }
    if(found) {
      callBack(true);
    } else {
      callBack(false);
    }
  })
}

function getUserInfo( token, callBack ) {
  checkAccess(token, function (passed) {
    if(passed){
      Users.findOne({token},'-_id -password -__v').populate('cart')
      .exec(callBack)
    } else {
      callBack(true)
    }
  })
}

function removeAccess( token, callBack ) {
  if(!token) {
    if(callBack) callBack(null);
    return;
  }
  Access.findOneAndRemove({token}).exec(function ( err, removed ) {
    if(err) {
      removeAccess( token, callBack );
      return;
    }
    if(callBack) callBack(removed);
  })
}

function dropAccess(callBack) {
  Access.remove({}).exec(callBack)
}

function adminAccess(token, admin, callBack){
  Users.findOne({token})
  .exec(function (err, user) {
    if(user){
      user.admin=admin;
      user.save(callBack)
    } else {
      callBack(true)
    }
  })
}
/// Access </>

module.exports.makeHash = makeHash;
module.exports.newAccess = newAccess;
module.exports.checkAccess = checkAccess;
module.exports.getUserInfo = getUserInfo;
module.exports.removeAccess = removeAccess;
module.exports.dropAccess = dropAccess;
module.exports.adminAccess = adminAccess;
