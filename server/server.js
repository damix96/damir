var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var router = express.Router();
var path =require('path');
const app = express()
var index = require('./routes/index')
var items = require('./routes/items')
const Port = 4000;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());

app.use('/api', index)
app.use('/items', items)

mongoose.connect('mongodb://localhost:27017/damir');
var dbc = mongoose.connection;
dbc.once('open', function() {
  console.log('Mongoose Connected');
});

app.use(express.static('static/'))


app.get('*', (req, res) => {
res.sendFile('index.html', { root: path.join(__dirname, '../static') });
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send({ message: err.message })
})

app.listen(Port, () => {
  console.log('server running on '+Port);
});

module.exports = app
