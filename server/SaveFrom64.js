const fs = require('fs');
const path = require('path');

exports.saveTo = function (input, filename, ext, callback) {
  filename = filename||'file';
  filename = path.join( process.cwd(), filename)
  if( typeof ext === 'function' ) {
    callback = ext;
    ext = null;
  }
  if(ext){
    if(ext[0] != '.'){
      ext= '.'+ext;
    }
  }
  var splited = input.split(',');
  
  fs.writeFile(filename+(ext||''), new Buffer(splited[1], "base64"), callback);
};
