import React from 'react';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';


import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

const customContentStyle = {
  width: 615,
  height: '100%',
  maxWidth: 'none',

};
class ModalDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };

  }


  componentDidMount(){
// bodyStyle={{overflowY: 'auto'}}
  }

  render() {
    const actions = [
      <FlatButton
        label="Купить"
        primary={true}
        keyboardFocused={false}

      />,
      <FlatButton
        label="Выход"
        primary={true}
        keyboardFocused={false}
        onClick={this.props.detailClose}
      />,
    ];
    return (
      <div>
        <Dialog
          title={`Подробная информация`}
          actions={actions}
          modal={true}
          open={this.props.open}
          contentStyle={customContentStyle}
          onRequestClose={this.props.detailClose}
          bodyStyle={{overflowY: 'auto'}}
        >
        <Card>

          <CardMedia
            overlay={<CardTitle title={"Продукт: "+this.props.data.title} subtitle={"Категория: "+this.props.data.category} />}
          >
            <img src={this.props.data.photo} alt="" />
          </CardMedia>
          <CardTitle title={"Цена за штуку: $"+this.props.data.price} subtitle={"Всего в наличии: "+this.props.data.count+'шт.'} />
          <CardText>
            Описание товара:<br/>

            {this.props.data.description}
          </CardText>
        </Card>
        </Dialog>
      </div>
    );
  }
}
export default ModalDetail;
