import React from 'react';
import Toggle from 'material-ui/Toggle';

class AccessSwitcher extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }



  render() {
    return (
      <div style={{
        height:65
      }}>
      <Toggle
      label="Admin mode"
      defaultToggled={this.props.admin}
      disabled={this.props.disabled}
      onToggle={this.props.onSwitch}
      labelStyle={{
        marginTop: -9
      }}
      style={{
        paddingTop: 15,
        maxWidth: 180
      }}
      />
      <div style={{

      }}/>
      </div>
    );
  }
}
export default AccessSwitcher;
