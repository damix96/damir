import React from 'react';
import axios from 'axios';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import SvgIcon from 'material-ui/SvgIcon';
import {blue500, red500, greenA200, white500} from 'material-ui/styles/colors';
const styles = {
  root: {
    display: 'flex',
    width: '100%',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '100%',
    // height: 600,
    marginTop: 20,
    overflowY: 'auto',
  },
  gridTile: {

  },
};

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };

  }

  render() {
    return (
      <div style={styles.root}>
        <GridList
          cellHeight={260}
          cols={4}
          padding={20}
          style={styles.gridList}
        >
        <Subheader style={{fontSize: 30}}>{this.props.subtitle}</Subheader>
          {this.props.items.map((tile, i) => (
            <GridTile
              className="card-1"
              key={tile._id}
              title={tile.title}
              titleStyle={{textAlign: 'left', fontSize:20, paddingBottom: 10}}
              subtitleStyle={{textAlign: 'left'}}
              subtitle={<div><span>Цена: <b> ${tile.price}</b></span><br/>
                        <span>Количество: <b>{tile.count}шт.</b></span></div>}
              actionIcon={
                <div>
              <IconButton style={{width:30, heigth:30,paddingRight: 30}} onClick={this.props.click.bind(this, tile, 1)}  tooltip={tile.inCart ? "Убрать из корзины" : "Добавить в корзину"} tooltipPosition="top-left">
                <SvgIcon>
                { tile.inCart ?
                  (<path fill="#fff" d="M3,10H6V7H3V10M5,5H8V2H5V5M8,10H11V7H8V10M17,1L12,6H15V10H19V6H22L17,1M7.5,22C6.72,22 6.04,21.55 5.71,20.9V20.9L3.1,13.44L3,13A1,1 0 0,1 4,12H20A1,1 0 0,1 21,13L20.96,13.29L18.29,20.9C17.96,21.55 17.28,22 16.5,22H7.5M7.61,20H16.39L18.57,14H5.42L7.61,20Z" />)
                  :
                  (<path id="cartButton" fill="#fff" d="M3,2H6V5H3V2M6,7H9V10H6V7M8,2H11V5H8V2M17,11L12,6H15V2H19V6H22L17,11M7.5,22C6.72,22 6.04,21.55 5.71,20.9V20.9L3.1,13.44L3,13A1,1 0 0,1 4,12H20A1,1 0 0,1 21,13L20.96,13.29L18.29,20.9C17.96,21.55 17.28,22 16.5,22H7.5M7.61,20H16.39L18.57,14H5.42L7.61,20Z" />)
                }
                </SvgIcon>
              </IconButton>
              {this.props.admin ?
              (<IconButton style={{width:30, heigth:30,paddingRight: 30}} onClick={this.props.click.bind(this, tile, 2)}  tooltip="Редактировать" tooltipPosition="top-left">
                <SvgIcon>
                  <path fill="#fff" d="M18.62,1.5C18.11,1.5 17.6,1.69 17.21,2.09L10.75,8.55L14.95,12.74L21.41,6.29C22.2,5.5 22.2,4.24 21.41,3.46L20.04,2.09C19.65,1.69 19.14,1.5 18.62,1.5M9.8,9.5L3.23,16.07L3.93,16.77C3.4,17.24 2.89,17.78 2.38,18.29C1.6,19.08 1.6,20.34 2.38,21.12C3.16,21.9 4.42,21.9 5.21,21.12C5.72,20.63 6.25,20.08 6.73,19.58L7.43,20.27L14,13.7" />
                </SvgIcon>
              </IconButton>) : null}
              {this.props.admin ?
              (<IconButton style={{width:30, heigth:30,paddingRight: 30}} onClick={this.props.click.bind(this, tile, 3)}  tooltip="Удалить товар" tooltipPosition="top-left">
                <SvgIcon>
                  <path id="cartButton3" fill="#fff" d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />
                </SvgIcon>
              </IconButton>) : null}
              </div>
              }
              style={styles.GridTile}
            >
              <img onClick={this.props.click.bind(this, tile, 5)} src={tile.photo} />
            </GridTile>

          ))}
          {
            (this.props.noAdd||!this.props.admin) ? <div/> :
            (<GridTile onClick={this.props.click.bind(this, null, 4)}
            title='Добавить новый товар'
            className="card-1"
            actionIcon={
            <IconButton style={{width:45, heigth:45,paddingRight: 30, marginRight: 25}}  tooltip="Добавить товар" tooltipPosition="top-center">
              <SvgIcon>
                <path fill="#fff" d="M13,9H18.5L13,3.5V9M6,2H14L20,8V20A2,2 0 0,1 18,22H6C4.89,22 4,21.1 4,20V4C4,2.89 4.89,2 6,2M11,15V12H9V15H6V17H9V20H11V17H14V15H11Z" />
              </SvgIcon>
            </IconButton>
            }
            >
            <img src="content/add.png" />
          </GridTile>)
        }
        </GridList>
      </div>
    );
  }
}

export default Card;
