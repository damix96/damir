import React from 'react';
import axios from 'axios';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import Access from './Access.jsx';
const styles = {
  title: {
    cursor: 'pointer',
  },
};


class AppBarExampleComposition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
    };
    this.handleNavigate = this.handleNavigate.bind(this)
    this.handleToggle = this.handleToggle.bind(this);

  }

  handleToggle() {
    console.log('asd');
    this.setState({open: !this.state.open});

  }

  handleNavigate(to, e){
    console.log(this.props);
    this.props.history.push(to);
    this.setState({open: false});
  };

  render() {
    return (
      <div>
        <Drawer docked={false} open={this.state.open} onRequestChange={this.handleToggle}>
        {
          this.props.onSwitch ? (
            <Access admin={this.props.user.admin} disabled={false} onSwitch={this.props.onSwitch}/>
          ) : (<div/>)
        }
          <MenuItem onClick={this.handleNavigate.bind(this, '/login')}>Sign in</MenuItem>
          <MenuItem onClick={this.handleNavigate.bind(this, '/')}>Все товары</MenuItem>
          <MenuItem onClick={this.handleNavigate.bind(this, '/cart')}>Корзина</MenuItem>
        </Drawer>
        <AppBar
          title={<span style={styles.title}>{this.props.title}</span>}
          onTitleClick={this.handleToggle}
          onLeftIconButtonClick={this.handleToggle}
          iconElementRight={!this.props.onSwitch ? (<div/>) : <FlatButton onClick={(function () {
            axios.get('/api/logout')
            .then( res => {
                this.props.history.push('/login');
            })
          }).bind(this)} label="Log Out" />}
        />
      </div>
    );
  }
}

export default AppBarExampleComposition;
