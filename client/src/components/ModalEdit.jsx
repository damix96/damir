import React from 'react';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import FileAs64 from './FileAs64.jsx'

class ModalEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      photo: null
    };
    this.submit = this.submit.bind(this);
    this.onFileLoad = this.onFileLoad.bind(this);
  }

  submit(e){
    var form = {};
    var { title, count, price, category, description } = document.getElementById('editModalForm');
    form.title = title.value;
    form.count = count.value;
    form.price = price.value;
    form.category = category.value;
    form.description = description.value;
    form.photo = this.state.photo;
    form._id = this.props.item._id
    let err = false;
    for (var key in form) {
      if (form.hasOwnProperty(key)) {
        if(form[key].length == 0) err=true;
      }
    }
    if(err){
      this.props.editError('Ошибка! Не все поля заполнены')
      return;
    }
    if( isNaN(form.price*1) ) {
      this.props.editError('Ошибка! "Цена" должна быть числом.  ')
      return;
    }
    if( isNaN(form.count*1) ) {
      this.props.editError('Ошибка! "Количество" должно быть числом.  ')
      return;
    }
    axios.post('/items/edit', form)
    .then( res => {
        if(!res.data.success){
          console.log('error',res.data.msg)
          this.props.editError(res.data.msg);
        } else {
          console.log('ok',res.data.msg)
          this.props.editClose(null, res.data.msg);
        }
    })
  }

  onFileLoad(event) {
    this.setState({photo: event.target.result})
  }

  componentDidMount(){

  }

  render() {
    const actions = [
      <FlatButton
        label="Готово"
        primary={true}
        keyboardFocused={false}
        onClick={this.submit}
      />,
      <FlatButton
        label="Выход"
        primary={true}
        keyboardFocused={false}
        onClick={this.props.editClose}
      />,
    ];
    var item = this.props.item||{};
    console.log(this.props.open)
    return (
      <div>
        <Dialog
          title={`Вы редактируете`}
          actions={actions}
          modal={true}
          open={this.props.open}
          bodyStyle={{overflowY: 'auto'}}
          onRequestClose={this.props.editClose}
        >
        <form id="editModalForm">
          <TextField
            floatingLabelText='Название'
            id="title"
            defaultValue={item.title}
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Количество'
            id="count"
            defaultValue={item.count}
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Цена'
            id="price"
            defaultValue={item.price}
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Категория'
            id="category"
            defaultValue={item.category}
            type="text"
            required={true}
          /><br />
          <TextField
            floatingLabelText='Описание'
            id="description"
            style={{width: 500}}
            multiLine={true}
            defaultValue={item.description}
            type="text"
          />
          <FileAs64 onFileLoad={this.onFileLoad}/>
          </form>
        </Dialog>
      </div>
    );
  }
}
export default ModalEdit;
