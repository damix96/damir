import React from 'react';
import axios from 'axios';
import TextField from 'material-ui/TextField';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    // {this.props.error ?
    // <div className="isa_error">
    //   {this.props.msg}
    // </div>
    // :
    // null
    // }
  }

  render() {
    if(!this.props.active){
      return null;
    }

    return (
      <div className="modal">
        <div className="modal-content">
          <span className="close" onClick={this.props.close}>&times;</span>
          <h2 style={{marginBottom: 10, marginRight:20}}className="modal-title" >{this.props.title}</h2>

          <form className="modal-wrapper" onSubmit={this.props.submit}>
            {
              this.props.inputs.map((item, i) => {
                return(
                  <div key={i}>
                  <TextField
                    floatingLabelText={item.title}
                    id={item.id}
                    defaultValue={item.value}
                    type={item.type}
                    required={item.required}
                    errorText={item.error}
                  /><br /> </div>
                )
              })
            }
          </form>
        </div>
      </div>
    );
  }
}

export default Modal;
