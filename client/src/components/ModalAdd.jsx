import React from 'react';
import axios from 'axios';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import FileAs64 from './FileAs64.jsx'

class ModalEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      photo: null,
      loading:false
    };
    this.submit = this.submit.bind(this);
    this.onFileLoad = this.onFileLoad.bind(this);
  }

  onFileLoad(event) {
    this.setState({photo: event.target.result})
  }

  submit(e){

    var form = {};
    var { title, count, price, category, description } = document.getElementById('editModalForm');
    form.title = title.value;
    form.count = count.value;
    form.price = price.value;
    form.category = category.value;
    form.photo = this.state.photo;
    form.description = description.value;
    let err = false;
    for (var key in form) {
      if (form.hasOwnProperty(key)) {
        if(form[key].length == 0&&key != 'photo') err=true;
      }
    }
    if(err){
      this.props.addError('Ошибка! Не все поля заполнены')
      return;
    }
    if( isNaN(form.price*1) ) {
      this.props.addError('Ошибка! "Цена" должна быть числом.  ')
      return;
    }
    if( isNaN(form.count*1) ) {
      this.props.addError('Ошибка! "Количество" должно быть числом.  ')
      return;
    }
    axios.post('/items/add', form)
    .then( res => {
        if(!res.data.success){
          console.log('error',res.data.msg)
          this.props.addError(res.data.msg);
        } else {
          console.log('ok',res.data.msg)
          this.props.addClose(null, res.data.msg);
        }
    })
  }

  componentDidMount(){

  }

  render() {
    const actions = [
      <FlatButton
        label="Добавить"
        primary={true}
        keyboardFocused={false}
        onClick={this.submit}
      />,
      <FlatButton
        label="Выход"
        primary={true}
        keyboardFocused={false}
        onClick={this.props.addClose}
      />,
    ];
    console.log(this.state, '-----------------');
    return (
      <div>
        <Dialog
          title={`Вы Добавляете`}
          actions={actions}
          modal={true}
          open={this.props.open}
          bodyStyle={{overflowY: 'auto'}}
          onRequestClose={this.props.addClose}
        >
        <form id="editModalForm">
          <TextField
            floatingLabelText='Название'
            id="title"
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Количество'
            id="count"
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Цена'
            id="price"
            type="text"
            required={true}
          />
          <TextField
            floatingLabelText='Категория'
            id="category"
            type="text"
            required={true}
          /><br />
          <TextField
            floatingLabelText='Описание'
            id="description"
            style={{width: 500}}
            multiLine={true}
            type="text"
          />
          <FileAs64 onFileLoad={this.onFileLoad}/>
          </form>
        </Dialog>
      </div>
    );
  }
}
export default ModalEdit;
