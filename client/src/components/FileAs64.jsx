import React from 'react';

var reader = new FileReader();
const MAX_FILE_SIZE = 50000;
class FileAs64 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      loading: false
    };
    this.openFile = this.openFile.bind(this);
    this.onLoadStart = this.onLoadStart.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onFileLoad = this.onFileLoad.bind(this);
    this.onLoadError = this.onLoadError.bind(this);
  }

  onLoadStart(event) {
    console.log('[Load Start]');
  }

  onProgress(event) {
    if(reader.total > MAX_FILE_SIZE){
      console.log('[File Too Large]', MAX_FILE_SIZE, reader.total);
      reader.abort()
    }
    console.log("['Progress']",event.loaded, event.total);
  }

  onFileLoad(event) {
    console.log("['Loaded']", {result:event.target.result});
  }

  onLoadError(event) {
    console.log("['Error']",event, reader);
  }

  abortLoad(event) {
    reader.abort()
  }

  onLoadAbort(event) {
    console.log("['Aborted']");
  }

  openFile(event) {
    var files = event.target.files //document.getElementById('file').files;
    if (files.length > 0) {
      console.log("['Files Chosed'] ", files);
      reader.readAsDataURL(files[0]);
    }
  }


  componentDidMount() {
    reader.onloadstart = this.props.onLoadstart||this.onLoadStart;
    reader.onload = this.props.onFileLoad||this.onFileLoad;
    reader.onprogress = this.props.onProgress||this.onProgress;
    reader.onerror = this.props.onLoadError||this.onLoadError;
    // reader.abort = this.props.abort||reader.abort;
    reader.onabort = this.props.onLoadAbort||this.onLoadAbort;
  }

  render() {
    
    return (<div>
      <input className={this.props.customClass} onChange={this.openFile} id="file" style={{
          width: 500
        }} type="file"/>
    </div>);
  }
}
export default FileAs64;
