import React from 'react';
import ReactDom from 'react-dom';
import { Redirect, BrowserRouter, Route, Link, History } from 'react-router-dom';
// import Auth from './modules/Auth';
import Login from './modules/Login.jsx';
import Catalog from './modules/Catalog.jsx';
import Cart from './modules/Cart.jsx';
import Header from './components/Header.jsx';
import FileAs64 from './components/FileAs64.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

ReactDom.render((
  <MuiThemeProvider>
  <BrowserRouter>
    <div>
      <Route exact path="/" component={Catalog}/>
      <Route path="/login" component={Login}/>
      <Route path="/cart" component={Cart}/>
      <Route path="/file" component={FileAs64}/>
    </div>
  </BrowserRouter>
  </MuiThemeProvider>
),
      document.getElementById('react-app'));
