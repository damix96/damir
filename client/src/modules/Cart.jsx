import React from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import Cards from '../components/Cards.jsx';
import Header from '../components/Header.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Snackbar from 'material-ui/Snackbar';
import ModalEdit from '../components/ModalEdit.jsx';
import ModalAdd from '../components/ModalAdd.jsx';
import SearchBar from 'material-ui-search-bar'

import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

var dynamicSort = (property) => {
  var sortOrder = 1;
  if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
  }
  return function (a,b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
  }
}

var sort = (arr, key) => {
  return arr.sort(dynamicSort(key))
}

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      add: false,
      staticItems: [],
      items: [],
      edit:false,
      msg: '',
      alert: false,
      value: 1,
      sortDropValue: 3,
      sortCatValue: 0,
      searchResult: [],
      searching: false,
      str: '-',
      sortOrder: ''
    };

    this.searchBarHandler = this.searchBarHandler.bind(this)
    this.keywordsList = this.keywordsList.bind(this);
    this.requester = this.requester.bind(this);
    this.editError = this.editError.bind(this);
    this.editClose = this.editClose.bind(this);
    this.click = this.click.bind(this);
    this.sortIconClick = this.sortIconClick.bind(this);
    this.categoryList = this.categoryList.bind(this);
    this.onSwitch = this.onSwitch.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.handleCatChange = this.handleCatChange.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSortDropChange = this.handleSortDropChange.bind(this);
    this.setSearchResult = this.setSearchResult.bind(this);
  }

  sortIconClick() {
    var {sortDropValue, sortOrder} = this.state;
    sortOrder = this.state.sortOrder==='-' ? '':'-';
    switch (sortDropValue) {
      case 1:
        this.setState({
          items: sort(this.state.items, this.state.sortOrder+'title'),
          sortDropValue,sortOrder
        })
        break;
        case 2:
          this.setState({
            items: sort(this.state.items, this.state.sortOrder+'count'),
            sortDropValue,sortOrder
          })
          break;
          case 3:
            this.setState({
              items: sort(this.state.items, this.state.sortOrder+'price'),
              sortDropValue,sortOrder
            })
            break;
            case 4:
              this.setState({
                items: sort(this.state.items, this.state.sortOrder+'category'),
                sortDropValue,sortOrder
              })
              break;
      default:
      this.setState({sortDropValue});
    }
  }

  setSearchResult( str ) {
    if(str.length == 0){
      this.setState({searching: false})
    }
    // str = this.state.str//str.toLowerCase();
    var searchResult = [];
    var items = this.state.items;
    switch (this.state.value) {
      case 1:
         items.map( item => {
          if(((item.title.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.title.toLowerCase()) > -1)))||
          ((item.description.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.description.toLowerCase()) > -1)))||
          ((item.category.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.category.toLowerCase()) > -1)))||
          ((item.price.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.price.toLowerCase()) > -1))))
          searchResult.push(item)
        })
        break;
      case 2:
      items.map( item => {
        if(((item.title.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.title.toLowerCase()) > -1))))
        searchResult.push(item)
      })
        break;
      case 3:
      items.map( item => {
        if(((item.description.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.description.toLowerCase()) > -1))))
        searchResult.push(item)
      })
        break;
      case 4:
      items.map( item => {
        if(((item.category.toLowerCase().indexOf(str) > -1)||((str.indexOf(item.category.toLowerCase()) > -1))))
        searchResult.push(item)
      })
        break;
      default:
      case 2:
      items.map( item => {
        if(((item.price.indexOf(str) > -1)||((str.indexOf(item.price) > -1))))
        searchResult.push(item)
      })
    }
    console.log(searchResult);
    return searchResult;
    // else {
    //   // this.setState({searchResult, searching: true, str})
    // }

  }

  searchBarHandler(str){
    this.setState({str, searching:true})
  }

  handleSortDropChange(event, index, sortDropValue) {
    switch (sortDropValue) {
      case 1:
        this.setState({
          items: sort(this.state.items, this.state.sortOrder+'title'),
          sortDropValue
        })
        break;
        case 2:
          this.setState({
            items: sort(this.state.items, this.state.sortOrder+'count'),
            sortDropValue
          })
          break;
          case 3:
            this.setState({
              items: sort(this.state.items, this.state.sortOrder+'price'),
              sortDropValue
            })
            break;
            case 4:
              this.setState({
                items: sort(this.state.items, this.state.sortOrder+'category'),
                sortDropValue
              })
              break;
      default:
      this.setState({sortDropValue});
    }
  }

  handleCatChange(event, index, sortCatValue) {
    console.log(index);
    switch (sortCatValue) {
      case 0:
          this.setState({sortCatValue, items: this.state.staticItems})
        break;
      default:
      var list = this.categoryList();
      var result = [];
      this.state.staticItems.forEach(item => {
        if(item.category == list[sortCatValue-1])
        result.push(item);
      })
      this.setState({sortCatValue, items: result})
    }
  }

  handleSearchChange(event, index, value) {
    this.setState({value})
  }

  categoryList() {
    var result = [];
    this.state.staticItems.forEach(function (item) {
      if( result.indexOf(item.category) == -1 )
      result.push(item.category)
    })
    return result;
  }

  keywordsList(categories) {
    var result = [];
    this.state.staticItems.forEach(function (item) {
      result = [
        ...result,
        ...item.title.split(' '),
        ...item.description.split(' '),
        ...(categories ? item.category.split(' ') : [])
      ]
    })
    console.log(result);
    return result;
  }

  requester(callback) {
    axios.get('/api/mydata')
    .then( res => {
      if(!res.data.success){
        this.props.history.push('/login');
      } else {
        var temp = res.data.data.cart
        temp.forEach(function (t) {
          t.inCart=true;
        })
        callback(
          {
            staticItems: temp,
            items: temp,
            user: res.data.data
          }
        )
      }
    })
  }

  editClose( e, msg ){
    console.log(msg, e, 'тут лог 2');
    var self=this;
    this.requester(function (data) {
      if(!e){
          data.alert = true
          data.msg = msg
      }
      data.edit=false;
      self.setState(data)
    });
  }

  editError(msg){
    this.setState({
      alert: true,
      msg
    })
  }

  handleRequestClose(){
    this.setState({
      alert:false
    })
  }

  click(tile, button, event){
    var self=this;
    switch (button) {
      case 1:
      axios.post('/items/tocart', {_id: tile._id})
      .then( res => {
        this.requester(function (data) {
          data.msg= res.data.msg;
          data.alert= true;
          self.setState(data)
        })
      })
        break;
      case 2:
        this.setState({
          editing: tile,
          edit: true
        })
        break;
      case 3:
      axios.post('/items/remove', {_id: tile._id})
      .then( res => {
        this.requester(function (data) {
          data.msg= res.data.msg;
          data.alert= true;
          self.setState(data)
        })
      })
        break;
      case 4:
        this.setState({
          add: true
        })
        break;
      default:

    }
    // this.props.history.push('/some/path')
  }

  onSwitch(e, status){
    console.log(status);
    axios.post('/api/switch', {admin: status})
    .then( res => {
      if(!res.data.success){
        this.props.history.push('/login');
      } else {
        this.setState({
          user: res.data.data,
          alert: true,
          msg: res.data.data.admin ? 'Теперь у вас права Администратора' : 'Теперь вы обычный юзер'
        })
      }
    })
  }

  componentDidMount() {
    var self=this
    this.requester(function (data) {
      self.setState(data)
    });
  }

  render() {
    console.log(this.state.edit)
    return (
      <div>
        <ModalEdit item={this.state.editing} open={this.state.edit} editClose={this.editClose} editError={this.editError}/>
        <Header history={this.props.history} user={this.state.user} title="Корзина" onSwitch={this.onSwitch} />
        <div className="top-padding"> </div>
        <div className="filters">
        <Toolbar>
          <ToolbarGroup firstChild={true}>
            <SearchBar
              dataSource={this.keywordsList(false)}
              onChange={this.searchBarHandler}
              onRequestSearch={console.log}
              style={{
                marginLeft: 40,
                maxWidth: 250
              }}
            />
            <DropDownMenu value={this.state.value} onChange={this.handleSearchChange}>
             <MenuItem value={1} primaryText="Везде" />
             <MenuItem value={2} primaryText="В названиях" />
             <MenuItem value={3} primaryText="В описании" />
             <MenuItem value={4} primaryText="В категориях" />
           </DropDownMenu>
           <ToolbarSeparator />
          </ToolbarGroup>
          <ToolbarGroup >
          <ToolbarTitle text="Сортировка" />
          <IconButton onClick={this.sortIconClick}>
            <FontIcon className={this.state.sortOrder==='-' ? "material-icons invert" : "material-icons"} style={{color: 'white'}}>sort</FontIcon>
          </IconButton>
            <DropDownMenu value={this.state.sortDropValue} onChange={this.handleSortDropChange}>
             <MenuItem value={1} primaryText="по названию" />
             <MenuItem value={2} primaryText="по количеству" />
             <MenuItem value={3} primaryText="по цене" />
             <MenuItem value={4} primaryText="по категориям" />
           </DropDownMenu>
           <ToolbarSeparator />
           <ToolbarTitle text="Показывать:" />
           <DropDownMenu value={this.state.sortCatValue} onChange={this.handleCatChange}>
           <MenuItem value={0} primaryText="Все" />
            {
              this.categoryList().map(function (category, i) {
                return (
                  <MenuItem key={i} value={i+1} primaryText={category} />
                )
              })
            }
          </DropDownMenu>
          </ToolbarGroup>
        </Toolbar>
        </div>
        <div className="cards">
                <Cards noAdd={true} items={this.state.searching ? (this.setSearchResult(this.state.str)):(this.state.items)} click={this.click} admin={this.state.user.admin}  />
        </div>

      <Snackbar
        open={this.state.alert}
        message={this.state.msg}
        autoHideDuration={4000}
        onRequestClose={this.handleRequestClose}
      />
      </div>
    );
  }
}

export default Cart;
