import React from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import Modal from '../components/Modal.jsx';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Header from '../components/Header.jsx';

const style = {
  height: 300,
  width: 330,
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
};
var inputs = [
  {
    title: 'Введите Email',
    value: '',
    id: 'login2',
    node: 'login',
    type: 'email',
    required: true,
    className: 'inputs'
  },
  {
    title: 'Придумайте пароль',
    value: '',
    id: 'password2',
    node: 'password',
    type: 'password',
    required: true,
    className: 'inputs'
  },
  {
    title: 'Повторите пароль',
    value: '',
    id: 'repassword',
    type: 'password',
    required: true,
    className: 'inputs'
  },
  {
    title: '',
    value: 'Регистрация',
    id: 'submit',
    type: 'submit',
    required: true,
    className: 'inputs'
  }
]

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      msg: '',
      errorReg: false,
      msgReg: '',
      redirect: false,
      redirectTo: ''
    };

    this.auth = this.auth.bind(this);
    this.reg = this.reg.bind(this);
    this.toggleRegModal = this.toggleRegModal.bind(this);
  }
  toggleRegModal(){
    this.setState({
      register: !this.state.register
    })
  }
  reg(e){
    e.preventDefault()
    var query = {};
    // inputs.map((item, i) => {
    //   if(item.node)
    //   query[item.node] = e.target[item.id].value;
    // })
    query.password = e.target.password2.value;
    query.login = e.target.login2.value;
    if(e.target.password2.value != e.target.repassword.value){
      inputs[inputs.length-2].error = 'Пароли не совпадают';
      this.setState({});
    } else {
      axios.post('api/reg', query)
      .then( res => {
        console.log('Modal =>',res.data)
        if(res.data.success){
          this.props.history.push('/')

        } else {
          inputs[inputs.length-1].error = res.data.msg;
          this.setState({
            errorReg: true,
            msgReg: res.data.msg
          })
        }

      })

    }

  }

  auth(e){
    const { login, password } = e.target;
    axios.post('/api/auth',  {
      login: login.value,
      password: password.value
    })
    .then( res => {
      console.log(res.data)
      if(res.data.success){
        this.props.history.push('/')
      } else {
        this.setState({
          error: true,
          msg: res.data.msg
        })
      }

    })
    e.preventDefault()
  }

  componentDidMount() {

  }

  render() {
    return (
      <div>
        <Header history={this.props.history} user={this.state.user} title="Авторизация" />
         <Paper style={style} zDepth={3} >
          <form onSubmit={this.auth}>
          <TextField
            floatingLabelText="Email"
            id="login"
            type="email"
          /><br />
          <TextField
            floatingLabelText="Password"
            id="password"
            type="password"
          /><br />
          <TextField
            id="go"
            type="submit"
            errorText={this.state.msg}
          /><br />
          </form>
          <br/>
          <a href="#" onClick={this.toggleRegModal}> Нет аккаунта? </a>
          </Paper>
          <Modal error={this.state.errorReg} msg={this.state.msgReg} submit={this.reg} close={this.toggleRegModal} title="Регистрация" active={this.state.register} inputs={inputs}/>
      </div>
    );
  }
}

export default Login;
