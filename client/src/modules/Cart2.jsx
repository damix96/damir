import React from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import Cards from '../components/Cards.jsx';
import Header from '../components/Header.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Snackbar from 'material-ui/Snackbar';
import ModalEdit from '../components/ModalEdit.jsx';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      add: false,
      items: [],
      edit:false,
      msg: '',
      alert: false
    };

    this.requester = this.requester.bind(this);
    this.editError = this.editError.bind(this);
    this.editClose = this.editClose.bind(this);
    this.click = this.click.bind(this);
    this.onSwitch = this.onSwitch.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this)
  }

  requester(callback) {
    axios.get('/api/mydata')
    .then( res => {
      if(!res.data.success){
        this.props.history.push('/login');
      } else {
        var temp = res.data.data.cart
        temp.forEach(function (t) {
          t.inCart=true;
        })
        callback(
          {
            items: temp,
            user: res.data.data
          }
        )
      }
    })
  }

  editClose(msg, e){
    var self=this;
    this.requester(function (data) {
      if(e){
          data.alert = true
          data.msg = msg
      }
      data.edit=false;
      self.setState(data)
    });
  }

  editError(msg){
    this.setState({
      alert: true,
      msg
    })
  }

  handleRequestClose(){
    this.setState({
      alert:false
    })
  }

  click(tile, button, event){
    var self=this;
    switch (button) {
      case 1:
      axios.post('/items/tocart', {_id: tile._id})
      .then( res => {
        this.requester(function (data) {
          data.msg= res.data.msg;
          data.alert= true;
          self.setState(data)
        })
      })
        break;
      case 2:
        this.setState({
          editing: tile,
          edit: true
        })
        break;
      case 3:
      axios.post('/items/remove', {_id: tile._id})
      .then( res => {
        this.requester(function (data) {
          data.msg= res.data.msg;
          data.alert= true;
          self.setState(data)
        })
      })
        break;
      case 4:
        this.setState({
          add: true
        })
        break;
      default:

    }
    // this.props.history.push('/some/path')
  }

  onSwitch(e, status){
    console.log(status);
    axios.post('/api/switch', {admin: status})
    .then( res => {
      if(!res.data.success){
        this.props.history.push('/login');
      } else {
        this.setState({
          user: res.data.data,
          alert: true,
          msg: res.data.data.admin ? 'Теперь у вас права Администратора' : 'Теперь вы обычный юзер'
        })
      }
    })
  }

  componentDidMount() {
    var self=this
    this.requester(function (data) {
      self.setState(data)
    });
  }

  render() {
    console.log(this.state.edit)
    return (
      <div>
        <ModalEdit item={this.state.editing} open={this.state.edit} editClose={this.editClose} editError={this.editError}/>
        <Header history={this.props.history} user={this.state.user} title="Моя корзина" onSwitch={this.onSwitch} />
      <div className="cards">
              <Cards items={this.state.items} click={this.click} admin={this.state.user.admin} noAdd={true}  />
      </div>
      <Snackbar
        open={this.state.alert}
        message={this.state.msg}
        autoHideDuration={4000}
        onRequestClose={this.handleRequestClose}
      />
      </div>
    );
  }
}

export default Login;
